#nextcloud 

```bash
docker run -d --name nextcloud -p 8182:80 --restart=always  -v /data/nextcloud:/var/www/html/data -d nextcloud
```

# prtainer
```bash
docker run -d -p 9000:9000 --restart=always -v /var/run/docker.sock:/var/run/docker.sock --name portainer portainer/portainer
```
# consul
```bash
docker run -d -p 8500:8500 \
	--restart=always \
	-v /data/consul:/consul/data \
	-e CONSUL_BIND_INTERFACE='eth0' \
	--name=consul1 consul agent -server -bootstrap -ui -client='0.0.0.0'
```	
# 集群节点
```bash
docker run -d --name=consul2 -e CONSUL_BIND_INTERFACE=eth0 consul agent --server=true --client=0.0.0.0 --join 172.17.0.2;
docker run -d --name=consul3 -e CONSUL_BIND_INTERFACE=eth0 consul agent --server=true --client=0.0.0.0 --join 172.17.0.2;
docker run -d --name=consul4 -e CONSUL_BIND_INTERFACE=eth0 consul agent --server=false --client=0.0.0.0 --join 172.17.0.2;
```